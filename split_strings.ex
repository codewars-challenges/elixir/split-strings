defmodule SplitStrings do
  def solution(str) do
    postfix = str |> String.length |> rem(2) |> underscores
    for <<x::binary-2 <- "#{str}#{postfix}">>, do: x
  end

  defp underscores(0), do: ""
  defp underscores(len), do: 1..len |> Enum.map(fn _ -> "_" end) |> Enum.join("")
end

IO.inspect SplitStrings.solution("a")
IO.inspect SplitStrings.solution("ab")
IO.inspect SplitStrings.solution("abc")